package com.itea.kazmin.application;

import java.io.IOException;
import java.util.Map;

public class ApplicationContext {

    public static String SHOPPING_COMMAND_DELIMETER;
    public static int SHOPPING_CART_INITIAL_ITEMS_COUNT = 5;
    public static int SHOPPING_CART_ITEMS_COUNT_EXPAND_STEP = 5;

    public static String SHOPPING_LIST_GREETING_TEXT;
    public static String SHOPPING_LIST_EXTRA_ADD_TEXT;
    public static String SHOPPING_LIST_THANK_YOU_TEXT;
    public static String SHOPPING_LIST_SHOPPING_STOP_TIP_TEXT;
    public static String SHOPPING_LIST_INPUT_YOU_SHOPPING_ITEM_TEXT;
    public static String SHOPPING_LIST_THATS_YOU_SHOPPING_LIST_TEXT;
    public static String SHOPPING_LIST_RESTRICTED_GOOD_WARNING_TEXT;
    public static String SHOPPING_LIST_EMPTY_GOOD_NAME_WARNING_TEXT;
    public static String SHOPPING_LIST_INPUT_ACCEPT_CODE;
    public static String SHOPPING_LIST_DELIMETER;
    public static String SHOPPING_LIST_COMMANDS_LIST_HEADER;
    public static String SHOPPING_LIST_INPUT_YOUR_COMMAND_TEXT;
    public static String SHOPPING_LIST_INPUT_GOOD_NAME_TEXT;
    public static String SHOPPING_LIST_BUY_MORE;
    public static String SHOPPING_CART_NO_ELEMENTS;
    public static String EXIT_APPLICATION_COMMAND;
    public static String ERROR_NO_ITEM_NAME_FOR_DELETE;
    public static String START_USER_COMMANd;

    public static String[] RESTRICTED_GOODS_NAMES_ARRAY = {"пиво", "chocolate", "шоколад", "beer", "пыво", "пивко", "сникерс", "баунти", "твикс"};
    public static String[] STOP_ADDDING_SHOPPING_LIST_ARRAY = {"стоп", "stop", "that's all"};
    public static Map<String, String> RESTRICTED_GOODS_NAME_MAP = Map.of(
            "Детский шоколад", "Молочный шоколад",
            "Хлеб", "Хлебцы диетические",
            "Мясо", "Свиная корейка без кости"
    );

    public static Map<String, String> COMMANDS = Map.of(
            "-add", "addItem",
            "-del_by_name", "delItemByName",
            "-del_by_index", "delItemByIndex",
            "-stop", "stopAddingItems",
            "-exit", "exitApplication",
            "-print", "printShoppingCart"
    );

    public ApplicationContext() {
        SHOPPING_LIST_GREETING_TEXT = "Скорее составляйте список покупок.";
        SHOPPING_LIST_EXTRA_ADD_TEXT = "Добавить что-то еще? Не забыли майонез? (Д/Н): ";
        SHOPPING_LIST_THANK_YOU_TEXT = "Спасибо за составление списка покупок, мы были счастливы помочь Вам. Пока!";
        SHOPPING_LIST_SHOPPING_STOP_TIP_TEXT = "Если хотите завершить добавление товаров в список, напишите любое слово из данного списка: ";
        SHOPPING_LIST_INPUT_YOU_SHOPPING_ITEM_TEXT = "Введите название товара: ";
        SHOPPING_LIST_THATS_YOU_SHOPPING_LIST_TEXT = "Это ваш список покупок: ";
        SHOPPING_LIST_RESTRICTED_GOOD_WARNING_TEXT = "Простите, но мы не можем добавить в список данную позицию с целью заботы о Вашем здоровье: ";
        SHOPPING_LIST_EMPTY_GOOD_NAME_WARNING_TEXT = "Простите, но вы не ввели название товара. Попробуйте еще раз.";
        SHOPPING_LIST_INPUT_ACCEPT_CODE = "д";
        SHOPPING_LIST_DELIMETER = String.valueOf(',');
        SHOPPING_COMMAND_DELIMETER = "\s";
        SHOPPING_LIST_COMMANDS_LIST_HEADER = "Это список команд для вашего списка покупок";
        SHOPPING_LIST_INPUT_YOUR_COMMAND_TEXT = "Введите команду: ";
        SHOPPING_LIST_INPUT_GOOD_NAME_TEXT = "Введите название товара: ";
        SHOPPING_LIST_BUY_MORE = "Пожалуйста, не уходите, ведь у нас можно купить все, что угодно!";
        SHOPPING_CART_NO_ELEMENTS = "В корзине товаров нет";
        EXIT_APPLICATION_COMMAND = "-exit";
        ERROR_NO_ITEM_NAME_FOR_DELETE = "Вы не ввели имя элемента для удаления";
        START_USER_COMMANd = "_";
    }

    public void clearScreen() {
        System.out.println("\n-----------------------------\n");
    }
}