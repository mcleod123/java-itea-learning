package com.itea.kazmin.application;

import com.itea.kazmin.exceptions.ShopWrongCommandException;

import java.util.Map;

import static com.itea.kazmin.application.ApplicationContext.*;


public class ApplicationCommands {

    private final Map<String, String> commands;

    public ApplicationCommands() {
        this.commands = COMMANDS;
        printCommandsListToConsole();
    }

    public void printCommandsListToConsole() {
        StringBuilder stringBuilder = new StringBuilder(SHOPPING_LIST_COMMANDS_LIST_HEADER)
                .append("\n");
        for (Map.Entry<String, String> entry : commands.entrySet()) {

            stringBuilder.append(entry.getKey())
                    .append(" -> ")
                    .append(entry.getValue())
                    .append("\n");

        }
        stringBuilder.append("-----------------------------\n");
        System.out.println(stringBuilder);
    }

    public String returnApplicationActionByCommand(String userInputString) {
        String applicationAction = "";
        for (Map.Entry<String, String> entry : commands.entrySet()) {
            if (userInputString.contains(entry.getKey())) {
                return entry.getValue();
            }
        }

        try {
            throw new ShopWrongCommandException("[INPUT COMMAND] User enter WRONG command.");
        } catch (ShopWrongCommandException e) {
            e.printStackTrace();
        }
        
        return applicationAction;
    }
}