package com.itea.kazmin;

import com.itea.kazmin.application.ApplicationCommands;
import com.itea.kazmin.application.ApplicationContext;
import com.itea.kazmin.exceptions.ShopUserInputExceptions;
import com.itea.kazmin.exceptions.ShopWrongCommandException;
import com.itea.kazmin.shop.ShoppingCart;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static com.itea.kazmin.application.ApplicationContext.*;
import static java.util.Objects.isNull;

public class MainApplication {

    private static final ApplicationContext applicationContext = new ApplicationContext();
    private static final ApplicationCommands applicationCommands = new ApplicationCommands();
    private static final ShoppingCart shoppingCart = new ShoppingCart();
    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    public static String userInput = START_USER_COMMANd;

    public static void main(String[] args) {
        MainApplication mainApplication = new MainApplication();
        MainApplication.startApplication(mainApplication);
    }

    private static void startApplication(MainApplication mainApplication) {
        while (isNull(userInput) || !userInput.equals(EXIT_APPLICATION_COMMAND)) {



            System.out.print(SHOPPING_LIST_INPUT_YOUR_COMMAND_TEXT);

            try {
                userInput = reader.readLine()
                        .trim();
            } catch (IOException e) {
                try {
                    throw new ShopUserInputExceptions("[INPUT DATA] Input data from user exception: " + userInput);
                } catch (ShopUserInputExceptions ex) {
                    ex.printStackTrace();
                }
            }

            if (userInput.equals("")) {
                try {
                    throw new ShopWrongCommandException("[INPUT COMMAND] User did not enter command.");
                } catch (ShopWrongCommandException e) {
                    e.printStackTrace();
                }
            }


            try {
                mainApplication.purchaseProcess(shoppingCart, userInput, applicationCommands, reader);
            } catch (IOException e) {
                try {
                    throw new ShopUserInputExceptions("Input data from user exception: " + userInput);
                } catch (ShopUserInputExceptions ex) {
                    ex.printStackTrace();
                }
            }
            applicationContext.clearScreen();



        }
    }

    public void purchaseProcess(
            ShoppingCart shoppingCart,
            String userInput,
            ApplicationCommands applicationCommands,
            BufferedReader reader
    ) throws IOException {

        String action = applicationCommands.returnApplicationActionByCommand(userInput);
        if (!action.equals("") && !userInput.equals(EXIT_APPLICATION_COMMAND)) {
            switch (action) {
                case ("addItem") -> {
                    System.out.print(SHOPPING_LIST_INPUT_GOOD_NAME_TEXT);
                    shoppingCart.addItem(reader.readLine());
                }
                case ("printShoppingCart") -> shoppingCart.printShoppingCart();
                case ("stopAddingItems") -> System.out.println(SHOPPING_LIST_BUY_MORE);
                case ("delItemByName") -> shoppingCart.delItemByName(userInput);
                case ("delItemByIndex") -> shoppingCart.delItemByIndex(userInput);
            }
        }
    }
}