package com.itea.kazmin.shop;

import static com.itea.kazmin.application.ApplicationContext.*;
import static java.util.Arrays.copyOf;

public class ShoppingCart {

    private ShoppingItem[] shoppingCart;
    private int itemsCount;
    private int maxItemsCount;

    public ShoppingCart() {
        this.shoppingCart = new ShoppingItem[SHOPPING_CART_INITIAL_ITEMS_COUNT];
        itemsCount = 0;
        maxItemsCount = SHOPPING_CART_INITIAL_ITEMS_COUNT;
    }

    public void addItem(String itemName) {
        shoppingCart[itemsCount] = new ShoppingItem(itemName);
        itemsCount++;
        if (itemsCount == maxItemsCount) {
            expandShoppingCart();
        }
    }

    public void printShoppingCart() {
        if (itemsCount == 0) {
            System.out.println(SHOPPING_CART_NO_ELEMENTS);
        }

        int tempItemListcounter = 1;
        System.out.println("---------------");
        for (ShoppingItem shoppingItem : shoppingCart) {
            if (shoppingItem != null) {
                System.out.println(tempItemListcounter + ". " + shoppingItem.getName());
                tempItemListcounter++;
            }
        }
    }

    private void expandShoppingCart() {
        maxItemsCount += SHOPPING_CART_ITEMS_COUNT_EXPAND_STEP;
        shoppingCart = copyOf(
                shoppingCart,
                maxItemsCount);

    }

    public void delItemByName(String userInput) {
        String[] userInputcommands = userInput.split(SHOPPING_COMMAND_DELIMETER);
        String deletedItemName = userInputcommands[1];
        if (deletedItemName != null) {
            int deletedItemIndex = getItemIndexByName(deletedItemName);
            removeElement(shoppingCart, deletedItemIndex);
        } else {
            System.out.println(ERROR_NO_ITEM_NAME_FOR_DELETE);
        }


    }

    public void delItemByIndex(String userInput) {

        String[] userInputcommands = userInput.split(SHOPPING_COMMAND_DELIMETER);
        int deletedItemIndex = Integer.parseInt(userInputcommands[1]);
        if (deletedItemIndex >= 0) {
            removeElement(shoppingCart, deletedItemIndex);
        } else {
            System.out.println(ERROR_NO_ITEM_NAME_FOR_DELETE);
        }
    }

    private int getItemIndexByName(String itemName) {

        int searchingItemIndex = 0;

        for (int i = 0; i < shoppingCart.length; i++) {
            if (shoppingCart[i] != null) {
                if (shoppingCart[i].getName().equals(itemName)) {
                    searchingItemIndex = i;
                    break;
                }
            }
        }
        return searchingItemIndex;
    }

    private void removeElement(ShoppingItem[] arr, int index) {
        ShoppingItem[] arrDestination = new ShoppingItem[arr.length - 1];
        int arrDestinationElementIndex = 0;
        for (int i = 0; i < shoppingCart.length; i++) {

            arrDestination[arrDestinationElementIndex] = shoppingCart[i];
            if (i != index) {
                arrDestinationElementIndex++;
            }
        }
        shoppingCart = arrDestination.clone();
    }
}