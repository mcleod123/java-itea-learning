package com.itea.kazmin.exceptions;



public class ShopUserInputExceptions extends Exception {

    private String errorMessage;

    public ShopUserInputExceptions(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }


}
