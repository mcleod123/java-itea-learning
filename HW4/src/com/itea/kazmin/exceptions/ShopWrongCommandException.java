package com.itea.kazmin.exceptions;

public class ShopWrongCommandException extends Exception  {

    private String errorMessage;

    public ShopWrongCommandException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

}